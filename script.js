const rockBox = document.getElementById("rock");
const paperBox = document.getElementById("paper");
const scissorBox = document.getElementById("scissor");
const rockBoxCom = document.getElementById("rockCom");
const paperBoxCom = document.getElementById("paperCom");
const scissorBoxCom = document.getElementById("scissorCom");
const resetButton = document.getElementById("reset");
const vsAnnoun = document.getElementById("vsAnnoun");
const playerAnnoun = document.getElementById("playerAnnoun");
const comAnnoun = document.getElementById("comAnnoun");
const drawAnnoun = document.getElementById("drawAnnoun");

let playerChoice = "";
let computerChoice = "";

const result = () => {
  if (
    (playerChoice === "rock" && computerChoice === "rockCom") ||
    (playerChoice === "paper" && computerChoice === "paperCom") ||
    (playerChoice === "scissor" && computerChoice === "scissorCom")
  ) {
    drawAnnoun.style.display = "";
    playerAnnoun.style.display = "none";
    comAnnoun.style.display = "none";
    vsAnnoun.style.display = "none";
    console.log("draw");
  } else if (
    (playerChoice === "rock" && computerChoice === "scissorCom") ||
    (playerChoice === "paper" && computerChoice === "rockCom") ||
    (playerChoice === "scissor" && computerChoice === "paperCom")
  ) {
    drawAnnoun.style.display = "none";
    playerAnnoun.style.display = "";
    comAnnoun.style.display = "none";
    vsAnnoun.style.display = "none";
    console.log("player win");
  } else {
    drawAnnoun.style.display = "none";
    playerAnnoun.style.display = "none";
    comAnnoun.style.display = "";
    vsAnnoun.style.display = "none";
    console.log("computer win");
  }
};

class Computer {
  pilihanCom = () => {
    const pilihanComputer = ["rockCom", "paperCom", "scissorCom"];
    const rnInt = Math.floor(Math.random() * pilihanComputer.length);
    const pilihan = pilihanComputer[rnInt];
    this.backgroundCom(pilihan);
    computerChoice = pilihan;
    result();
  };
  backgroundCom = (terpilih) => {
    const comClick = document.getElementById(terpilih);
    comClick.style.backgroundColor = "#C4C4C4";
  };
}

const com = new Computer();

rockBox.onclick = () => {
  rockBox.style.backgroundColor = "#C4C4C4";
  disabledButton();
  console.log("Batu telah dipilih player");
  playerChoice = "rock";
  com.pilihanCom();
};

paperBox.onclick = () => {
  paperBox.style.backgroundColor = "#C4C4C4";
  disabledButton();
  console.log("Kertas telah dipilih player");
  playerChoice = "paper";
  com.pilihanCom();
};

scissorBox.onclick = () => {
  scissorBox.style.backgroundColor = "#C4C4C4";
  disabledButton();
  console.log("Gunting telah dipilih player");
  playerChoice = "scissor";
  com.pilihanCom();
};

disabledButton = () => {
  rockBox.style.pointerEvents = "none";
  paperBox.style.pointerEvents = "none";
  scissorBox.style.pointerEvents = "none";
};

resetButton.onclick = () => {
  rockBox.style.backgroundColor = "#9c835f";
  paperBox.style.backgroundColor = "#9c835f";
  scissorBox.style.backgroundColor = "#9c835f";
  rockBox.style.pointerEvents = "";
  paperBox.style.pointerEvents = "";
  scissorBox.style.pointerEvents = "";
  rockBoxCom.style.backgroundColor = "#9c835f";
  paperBoxCom.style.backgroundColor = "#9c835f";
  scissorBoxCom.style.backgroundColor = "#9c835f";
  console.log("Ulang permainan");
  drawAnnoun.style.display = "none";
  playerAnnoun.style.display = "none";
  comAnnoun.style.display = "none";
  vsAnnoun.style.display = "";
};
